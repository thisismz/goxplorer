#!/bin/sh

read web_url avatar_url << EOCURL
$(curl -s "${BASEURL}"| jq -r '.web_url + " " + .avatar_url')
EOCURL
link="<a href=\"${web_url}\" class=\"lead\">&nbsp;&gt; Go to project's Gitlab</a>"
rm -rf public
mkdir -p public
cp -f pages/*.html pages/*.css pages/bitcoin.png public/
alt="Goxplorer gopher!"
sed "3 a <img width=\"40\" alt=\"${alt}\" src="${avatar_url}"> ${link}\n" \
	README.md >public/README.md
