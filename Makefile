PROGNAME=goxplorer
OUTPUT=${PROGNAME}_${CI_COMMIT_TAG}_${GOOS}_${GOARCH}

all:
	go build
cross:
	go build -o ${OUTPUT}
	sha256sum ${OUTPUT} >${OUTPUT}.sha256
test:
	# limit parallel tests because leveldb can't be accessed concurrently
	go test -v ./... -p 1
coverage:
	go test ./... -p 1 -covermode=count -coverprofile=coverage.out
	go tool cover -func=coverage.out
coverhtml:
	go tool cover -html=coverage.out
restoreindex:
	rm -rf samples/testindex/* samples/testchainstate/*
	tar zxvf testindex_orig.tgz
	tar zxvf testchainstate_orig.tgz
