// Naive bruteforcer to try discovering private keys against dictionary files

package callbacks

import (
	"bufio"
	"crypto/sha256"
	"fmt"
	"log"
	"os"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil"
	"gitlab.com/iMil/goxplorer/blockchain"
)

func genaddr(k string) (string, string) {
	key := sha256.Sum256([]byte(k))

	privKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), key[:])

	wif, _ := btcutil.NewWIF(privKey, &chaincfg.MainNetParams, false)
	pubaddr, _ := btcutil.NewAddressPubKey(wif.SerializePubKey(), &chaincfg.MainNetParams)
	return wif.String(), pubaddr.EncodeAddress()
}

// Bruteforce tries to discover private keys by converting words from a
// dictionary given as a parameter to a private/public key pair
func Bruteforce(b blockchain.Block) {

	file, err := os.Open(blockchain.Params.BFuncParam)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	pos := 1

	for scanner.Scan() {
		if pos%500 == 0 {
			fmt.Fprintf(os.Stderr, "%d lines processed\n", pos)
		}
		word := scanner.Text()
		pvt, addr := genaddr(word)
		for _, t := range b.Transac {
			for _, out := range t.Outs {
				if len(out.Address) == 0 {
					continue
				}
				if out.Address == addr {
					fmt.Printf("%s, %s, %s (%s)\n", word, pvt, addr, t.Txid)
				}
			}
		}
		pos++
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
