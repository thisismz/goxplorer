#!/bin/sh

# test the program itself

PROG=./goxplorer

blkfile=1845
export BTCBLOCKSHOME=samples
export BTCBLOCKINDEX=samples/testindex

# blk01845.dat

# block hash
wblock=000000000000000000147a7c7d848682df32f5c65e321f444ef7f18904f9a1f2
# block height
wheight=601629
# raw serialized transaction
wtx="010000000001010000000000000000000000000000000000000000000000000000000000000000ffffffff64031d2e092cfabe6d6dfd4ad76684d950f0d552c65b65f4327f8cdc59dabf5667188a7a5af5945d29cf10000000f09f909f00104d696e6564206279207768787a383838000000000000000000000000000000000000000000000000000000050060290000000000000462c9044c000000001976a914c825a1ecf2a6830c4401620c3a16f1995057c2ab88ac00000000000000002f6a24aa21a9ed5234157ea6c81de0dbf0ced1969164328c4992e2f895deadbb1da2d75af1d23b08000000000000000000000000000000002c6a4c2952534b424c4f434b3a4c26568b3ec880eff3c406ace7c18d27cdd710de2c4e5f7c590fb22e001bf1730000000000000000266a24b9e11b6df3af861a7b874ec0565192fd6cc88e56fb1310d2ad37ca999da9112c0b8b72b101200000000000000000000000000000000000000000000000000000000000000000bd1c4c3a"
# transaction id
wtxid=df882c43a0441b2f9b14ffd39738a6a36cda3b8c11279c7d0f93ccd5002c9482
# first output address
waddr=1KFHE7w8BhaENAswwryaoccDb6qcT6DbYY

echo -n "testing blk file read.. "
block=$(${PROG} -b ${blkfile} -x -j|jq -r '.[].blockhash')
if [ "$block" != "$wblock" ]; then
	echo "wrong block hash"
	exit 1
fi
echo done
echo -n "testing block hash read.. "
height=$(${PROG} -l ${block} -j -n 1|jq -r '.[].transacdata[0].inputs[0].height')
if [ "$height" != "$wheight" ]; then
	echo "wrong block height"
	exit 1
fi
echo done
echo -n "testing height read.. "
tx=$(${PROG} -e ${height} -n 1 -j -r|jq -r '.[].transacdata[0].raw'|base64 -d|xxd -p|tr -d '\n')
if [ "$tx" != "$wtx" ]; then
	echo "wrong transaction data"
	exit 1
fi
echo done
echo -n "testing transaction read.. "
txid=$(${PROG} -tx ${tx} -t -j|jq -r '.[].transacdata[0].txid')
if [ "$txid" != "$wtxid" ]; then
	echo "wrong txid"
	exit 1
fi
echo done
echo -n "testing date read.. "
addr=$(${PROG} -d 2019-10-30 -n 1 -t -a -j|jq -r ".[].transacdata[]|select(.txid == \"$txid\")|.outputs[0].address")
if [ "$addr" != "$waddr" ]; then
	echo "wrong address"
	exit 1
fi
echo done

exit 0
