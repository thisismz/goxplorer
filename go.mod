module gitlab.com/iMil/goxplorer

go 1.14

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/btcsuite/btcutil v1.0.2
	github.com/dgraph-io/badger v1.6.1
	github.com/dgraph-io/badger/v2 v2.0.3
	github.com/julienschmidt/httprouter v1.3.0
	github.com/syndtr/goleveldb v1.0.0
)
