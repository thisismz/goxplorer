/*
 * Goxplorer, a blockchain file explorer
 * Copyright (C) 2019 Emile `iMil" Heitor
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package blockchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/btcsuite/btcutil"
	"github.com/btcsuite/btcutil/base58"
	"github.com/btcsuite/btcutil/bech32"
)

// BlockHeader stores block's headers
type BlockHeader struct {
	Version    uint32 `json:"version"`
	PrevHash   string `json:"prevhash"`
	MerkleRoot string `json:"merkleroot"`
	TimeStamp  uint32 `json:"timestamp"`
	Target     uint32 `json::"target"`
	Nonce      uint32 `json:"nonce"`
}

// Inputs / spending structure https://learnmeabitcoin.com/guide/input
// Plus custom fields (Coinbase, Height, Raw and LockType)
type Inputs struct {
	PrevTxid string   `json:"prevtxid"` // Previous (output) TXID
	PrevOut  uint32   `json:"prevout"`  // Previous output number
	SSigSize uint64   `json:"ssigsize"`
	SSig     []byte   `json:"ssig"`
	Sequence uint32   `json:"sequence"`
	Address  []string `json:"address,omitempty"`
	Coinbase bool     `json:"coinbase"`
	Height   uint32   `json:"height,omitempty"`
	Witness  [][]byte `json:"witness,omitempty"`
	Raw      []byte   `json:"raw,omitempty"`
	LockType string   `json:"locktype,omitempty"`
}

// Outputs https://learnmeabitcoin.com/guide/output
// Plus custom fields (LockType and Raw)
type Outputs struct {
	Value       uint64 `json:"value"`
	SPubKeySize uint64 `json:"spubkeysize"`
	SPubKey     []byte `json:"spubkey"`
	Address     string `json:"address,omitempty"`
	LockType    string `json:"locktype,omitempty"`
	Raw         []byte `json:"raw,omitempty"`
}

// TransacData holds the transaction data
// https://learnmeabitcoin.com/guide/transaction-data
type TransacData struct {
	Version  uint32    `json:"version"`
	InCount  uint64    `json:"incount"`
	Ins      []Inputs  `json:"inputs"`
	OutCount uint64    `json:"outcount"`
	Outs     []Outputs `json:"outputs"`
	LockTime uint32    `json:"locktime"`
	Raw      []byte    `json:"raw,omitempty"`
	Txid     string    `json:"txid,omitempty"`
	SegWit   bool      `json:"segwit"`
	WTxid    string    `json:"wtxid,omitempty"`
}

// Block holds the full block structure
type Block struct {
	Magic     uint32        `json:"magic"`
	Size      uint32        `json:"size"`
	Header    BlockHeader   `json:"header"`
	TxCount   uint64        `json:"txcount"`
	Transac   []TransacData `json:"transacdata"`
	BlockHash string        `json:"blockhash,omitempty"`
}

// SParams holds block loading parameters
type SParams struct {
	NBlocks       uint32      `json:"nblocks"`       // number of blocks to load, 0 for all
	StartBlock    uint32      `json:"startblock"`    // block number to start loading at
	StartHash     uint64      `json:"starthash"`     // block hash to start reading at
	P2WPrefix     string      `json:"p2wprefix"`     // SegWit address prefix
	LoadAddr      bool        `json:"loadaddr"`      // load (output) addresses
	LoadInAddr    bool        `json:"loadinaddr"`    // load / guess input addresses
	LoadTxid      bool        `json:"loadtxid"`      // compute and load TXIDs
	LoadBlockHash bool        `json:"loadblockhash"` // compute and load current block hash
	LoadRaw       bool        `json:"loadraw"`       // load in and out raw data
	TransacFunc   interface{} `json:"transacfunc"`   // optional transactions helper function
	TFuncParam    string      `json:"tfuncparam"`    // optional parameter for transaction callback
	BlockFunc     interface{} `json:"blockfunc"`     // optional block load helper function
	BFuncParam    string      `json:"bfuncparam"`    // optional parameter for block callback
	AllBlocksFunc interface{} `json:"allblocksfunc"` // optional function to process all blocks
	ABFuncParam   string      `json:"abfuncparam"`   // optional parameter for all blocks callback
}

const (
	OP_DUP           = 0x76 // duplicates the top stack item
	OP_HASH160       = 0xa9 // SHA-256 and RIPEMD-160 hashed input
	OP_EQUALVERIFY   = 0x88 // inputs equality + verify result
	OP_CHECKSIG      = 0xac // valid signature for this hash and public key
	OP_1             = 0x51 // 1 / True
	OP_3             = 0x53 // 3
	OP_CHECKMULTISIG = 0xae // compares signatures
	OP_EQUAL         = 0x87 // checks inputs equality
	OP_RETURN        = 0x6a // no-op, invalid transaction
	OP_PUSHDATA1     = 0x4c // next byte contains the number of bytes to push
	pkUsize          = 65   // public key uncompressed size
	pkCsize          = 33   // public key compressed size
	scriptPubKey     = 0
	scriptSig        = 1
	p2pkhsig         = 0x00
	p2shsig          = 0x05 // P2SH signature to produce 3xxx addresses
	p2wsh            = 0x0
	p2wpkh           = 0x1
	p2err            = 0xff
)

var (
	cmagic = map[string]uint32{
		"BTC": 0xd9b4bef9,
	}
	// Params are exported parameters to enable loading features
	Params = SParams{
		NBlocks:       0, // load all blocks
		StartBlock:    0, // start at block 0
		P2WPrefix:     "bc",
		LoadAddr:      false,
		LoadInAddr:    false,
		LoadTxid:      false,
		LoadBlockHash: false,
		LoadRaw:       false,
		TransacFunc:   nil,
		BlockFunc:     nil,
		AllBlocksFunc: nil,
	}
)

// Binary.LittleEndian can't reverse byte slices, hence this helper function
func reverse(s []byte) []byte {
	size := len(s)
	x := make([]byte, size)
	copy(x, s) // s is a byte slice (i.e. pointer), modifying it modifies source
	for i, j := 0, size-1; i < j; i, j = i+1, j-1 {
		x[i], x[j] = x[j], x[i]
	}
	return x
}

// Re-read data and return raw bytes format
func readRaw(buf *bytes.Reader, pos int) ([]byte, error) {
	rsiz := pos - buf.Len()
	// rewind to read input
	_, err := buf.Seek(int64(-(rsiz)), io.SeekCurrent)
	if err != nil {
		return nil, err
	}
	raw := make([]byte, rsiz)
	// record raw input
	_, err = buf.Read(raw) // record raw input data
	if err != nil {
		return nil, err
	}
	return raw, nil
}

// Hash160ToAddress converts a Hash160 to an actual address
// Adapted from https://strm.sh/post/bitcoin-address-generation/
func Hash160ToAddress(pk []byte, version byte) []byte {
	a := append([]byte{version}, pk...)
	h := sha256.Sum256(a)
	chksum := sha256.Sum256(h[:])
	return append(a, chksum[:4]...)
}

// ReadHeader reads block header
func (b *Block) ReadHeader(header []byte) {
	b.Header.Version = binary.LittleEndian.Uint32(header[:4])
	b.Header.PrevHash = hex.EncodeToString(reverse(header[4:36]))
	b.Header.MerkleRoot = hex.EncodeToString(reverse(header[36:68]))
	b.Header.TimeStamp = binary.LittleEndian.Uint32(header[68:72])
	b.Header.Target = binary.LittleEndian.Uint32(header[72:76])
	b.Header.Nonce = binary.LittleEndian.Uint32(header[76:80])
}

// ValidPkSize Determines if this is a valid public key size,
// uncompressed or compressed
func ValidPkSize(size int, s byte) bool {
	if (size == pkUsize && s == 0x04) || // uncompressed
		(size == pkCsize && (s == 0x02 || s == 0x03)) { // compressed
		return true
	}
	return false
}

// MkWitAddr creates a valid bech32 address from []byte
func MkWitAddr(s []byte) (string, error) {
	conv, err := bech32.ConvertBits(s, 8, 5, true)
	if err != nil {
		return "", err
	}
	// prepending 0x0 is needed to get the desired SegWit format
	// https://bitcointalk.org/index.php?topic=4992632.0
	conv = append([]byte{0x0}, conv...)

	return bech32.Encode(Params.P2WPrefix, conv)
}

// Pk2Address converts public key from lock / scriptPubKey to plain address
func Pk2Address(s []byte) ([]string, string, error) {
	l := len(s)

	address := make([]string, 1)
	var locktype string
	var err error

	// invalid transaction
	if s[0] == OP_RETURN {
		address[0] = hex.EncodeToString(s)
		locktype = "OP_RETURN"
		// P2PKH, 1 bytes (size) + 20 bytes (HASH160) + 4 OPs
	} else if l == 25 && s[0] == OP_DUP && s[1] == OP_HASH160 &&
		s[l-2] == OP_EQUALVERIFY && s[l-1] == OP_CHECKSIG {
		// p2pkhsig version == 0x0 for regular p2pkh addresses 1...
		address[0] = base58.Encode(Hash160ToAddress(s[3:l-2], p2pkhsig))
		locktype = "P2PKH"
		// P2SH, OP + size + HASH160 + OP_EQUAL
		// https://en.bitcoin.it/wiki/BIP_0013
		// p2shsig version == 0x05 for P2SH addresses 3...
	} else if l == 23 && s[0] == OP_HASH160 && s[l-1] == OP_EQUAL {
		address[0] = base58.Encode(Hash160ToAddress(s[2:l-1], p2shsig))
		locktype = "P2SH"
		// P2PK, size + pk + OP
	} else if l > 1 && ValidPkSize(int(s[0]), s[1]) && s[l-1] == OP_CHECKSIG {
		h := btcutil.Hash160(s[1 : l-1])
		address[0] = base58.Encode(Hash160ToAddress(h, p2pkhsig))
		locktype = "P2PK"
		// P2MS, OP + size + pk + 2 OP, n times
	} else if l > pkCsize && ValidPkSize(int(s[1]), s[2]) &&
		s[l-1] == OP_CHECKMULTISIG {
		npk := int(s[l-2] - 80) // number of keys (0 is "required"), OP_1 == 81
		address = make([]string, npk)
		offset := 1 // pass the opcode
		for i := 0; i < npk; i++ {
			ksize := int(s[offset]) // key size, 65 or 33
			offset++
			// key size is greater than remaining size, shouldn't happen, but
			// happens on blk00112.dat
			if ksize > len(s[offset:]) {
				break
			}
			h := btcutil.Hash160(s[offset : offset+ksize])
			address[i] = base58.Encode(Hash160ToAddress(h, p2shsig))
			offset += ksize
		}
		locktype = "P2MS"
		// P2WPKH, 0x0 + size + HASH169 - SegWit
	} else if l == 22 && s[0] == 0x0 && s[1] == 0x14 {
		address[0], err = MkWitAddr(s[2:])
		if err != nil {
			return nil, locktype, err
		}
		locktype = "P2WPKH"
		// P2WSH
	} else if l == 34 && s[0] == 0x0 && s[1] == 0x20 {
		address[0], err = MkWitAddr(s[2:])
		if err != nil {
			return nil, locktype, err
		}
		locktype = "P2WSH"
	}
	// witness root hash, BIP 141, covered by OP_RETURN
	/*
		} else if l >= 38 &&
			bytes.Equal(s[0:6], []byte{0x6a, 0x24, 0xaa, 0x21, 0xa9, 0xed}) {
			address[0] = hex.EncodeToString(s)
		}
	*/

	return address, locktype, nil
}

// ReadVarint returns a VarInt value and the varint size
// https://learnmeabitcoin.com/guide/varint
// https://bitcoin.org/en/developer-reference#compactsize-unsigned-integers
// This is not the same varint as in base 128 varint
func ReadVarint(buf *bytes.Reader) (uint64, int, error) {
	vint, err := buf.ReadByte()
	if err != nil {
		return 0, 0, err
	}

	var vint2 uint16
	var vint4 uint32
	var vint8 uint64

	switch vint {
	case 0xfd:
		err = binary.Read(buf, binary.LittleEndian, &vint2)
		if err != nil {
			return 0, 0, err
		}
		return uint64(vint2), 3, nil // 2 + varint prefix
	case 0xfe:
		err = binary.Read(buf, binary.LittleEndian, &vint4)
		if err != nil {
			return 0, 0, err
		}
		return uint64(vint4), 5, nil // 4 + varint prefix
	case 0xff:
		err = binary.Read(buf, binary.LittleEndian, &vint8)
		if err != nil {
			return 0, 0, err
		}
		return uint64(vint8), 9, nil // 8 + varint prefix
	default:
		return uint64(vint), 1, nil
	}
}

// SSigToPk extracts public key from Script Sig P2PKH type
func SSigToPk(s []byte, ssize uint64) ([]byte, error) {
	buf := bytes.NewReader(s)

	// read script size
	siglen, off1, err := ReadVarint(buf)
	if err != nil {
		return nil, err
	}
	// mnimum ssize should be siglen + compressed pubkey
	if siglen+pkCsize > ssize {
		return nil, nil
	}
	readbytes := make([]byte, siglen)
	// not used, we don't process the script
	_, err = buf.Read(readbytes[:siglen])
	if err != nil {
		return nil, err
	}
	// read public key size
	pklen, off2, err := ReadVarint(buf)
	if err != nil {
		return nil, err
	}
	// public key len is not valid
	if siglen+pklen+uint64(off1+off2) != ssize { // +2 are the two varint sizes
		return nil, nil
	}
	// we've got room for reading
	readbytes = make([]byte, pklen)
	_, err = buf.Read(readbytes[:pklen])
	if err != nil {
		return nil, err
	}
	if !ValidPkSize(int(pklen), readbytes[0]) {
		return nil, nil
	}

	return readbytes[:pklen], nil
}

// SSig2Addr Exctract addresses from scriptSig when possible
func SSig2Addr(in *Inputs) error {
	var i, start, size int
	var sigtype byte
	a := make([]string, 1)

	size = int(in.SSigSize)

	// PK2PKH
	pk, err := SSigToPk(in.SSig, in.SSigSize)
	if err != nil {
		return err
	}
	if pk != nil {
		a[0] = base58.Encode(Hash160ToAddress(btcutil.Hash160(pk), p2pkhsig))
		in.Address = a
		in.LockType = "P2PKH"
		sigtype = p2pkhsig
		// multisig P2SH
		// the following is more of a guess as we rely on OP_CHECKMULTISIG's bug
		// which makes le leading 0x0 mandatory.
		// From https://bitcoin.stackexchange.com/questions/91825/non-segwit-p2sh-bytes-structure/91853#91853
		// we are not supposed to deduce addresses from SigScript
	} else if size > 2 && in.SSig[0] == 0x0 {
		for i = 1; i+int(in.SSig[i])+1 < int(size) &&
			// signatures before redeem script end with 0x01
			in.SSig[i+int(in.SSig[i])] == 0x01; i += int(in.SSig[i]) + 1 {
		}
		sigtype = p2shsig
		start = i + 2 // + 2 OPcodes
		in.LockType = "P2SH"
		// P2WSH nested in BIP16 P2SH 0x220020{32-byte-hash}
	} else if size == 35 && bytes.Equal(in.SSig[:3], []byte{0x22, 0x00, 0x20}) {
		sigtype = p2shsig
		start = 1
		in.LockType = "P2WSH-P2SH"
		// P2WPKH nested in P2SH 0x160014{20-byte-key-hash}
	} else if size == 23 && bytes.Equal(in.SSig[:3], []byte{0x16, 0x00, 0x14}) {
		sigtype = p2shsig
		start = 1
		in.LockType = "P2WPKH-P2SH"
		// classic P2PKH
	}
	// P2SH hashing
	if sigtype == p2shsig {
		h := btcutil.Hash160(in.SSig[start:]) // pass OPs + size
		a[0] = base58.Encode(Hash160ToAddress(h, sigtype))
		in.Address = a
	}

	return nil
}

// ReadInputs reads and returns `incount` number of inputs
func (b *Block) ReadInputs(incount uint64, buf *bytes.Reader) ([]Inputs, error) {
	var i uint64

	// allocate Inputs
	inputs := make([]Inputs, incount)
	readbytes := make([]byte, 32) // 256 bits for txid
	for i = 0; i < incount; i++ {
		pos := buf.Len() // record current position for forther raw record
		// read transaction id, 32 bits
		_, err := buf.Read(readbytes[:32])
		if err != nil {
			return nil, err
		}
		inputs[i].PrevTxid = hex.EncodeToString(reverse(readbytes[:32]))
		coinbase := make([]byte, 32)
		if bytes.Equal(readbytes[:32], coinbase) {
			inputs[i].Coinbase = true
		} else {
			inputs[i].Coinbase = false
		}
		// read 4 bytes Vector output or ffffffff for coinbase
		err = binary.Read(buf, binary.LittleEndian, &inputs[i].PrevOut)
		if err != nil {
			return nil, err
		}
		// variable int for script signature
		inputs[i].SSigSize, _, err = ReadVarint(buf)
		if err != nil {
			return nil, err
		}
		vsiz := inputs[i].SSigSize // shorter to type
		if vsiz > 0 {              // non native SegWit
			inputs[i].SSig = make([]byte, vsiz)
			// first read the script signature
			_, err := buf.Read(inputs[i].SSig)
			if err != nil {
				return nil, err
			}
			// coinbase's script sig starts with block's height
			// https://github.com/bitcoin/bips/blob/master/bip-0034.mediawiki
			if inputs[i].Coinbase && b.Header.Version > 1 {
				// from https://bitcoin.org/en/developer-reference
				// The data-pushing opcode will be 0x03 and the total size four
				// bytes until block 16,777,216 about 300 years from now.
				height := append([]byte{0x0}, reverse(inputs[i].SSig[1:4])...)
				inputs[i].Height = binary.BigEndian.Uint32(height)
			}
			// load input addresses
			if Params.LoadInAddr && !inputs[i].Coinbase {
				inputs[i].Address = make([]string, 1)
				err = SSig2Addr(&inputs[i])
				if err != nil {
					return nil, err
				}
			}

		}
		// sequence
		err = binary.Read(buf, binary.LittleEndian, &inputs[i].Sequence)
		if err != nil {
			return nil, err
		}
		if Params.LoadRaw {
			inputs[i].Raw, err = readRaw(buf, pos)
			if err != nil {
				return nil, err
			}
		}
	} // for incount
	return inputs, nil
}

// ReadOutputs read and returns `outcount` number of outputs
func (b *Block) ReadOutputs(outcount uint64, buf *bytes.Reader) ([]Outputs, error) {
	var i uint64

	outputs := make([]Outputs, outcount)
	var address []string

	for i = 0; i < outcount; i++ {
		pos := buf.Len() // record current position for further raw record
		// read value in Satoshis, 8 bytes
		err := binary.Read(buf, binary.LittleEndian, &outputs[i].Value)
		if err != nil {
			return nil, err
		}
		// read script pubKey size, varint (but mostly 1 byte)
		outputs[i].SPubKeySize, _, err = ReadVarint(buf)
		if err != nil {
			return nil, err
		}
		pksize := outputs[i].SPubKeySize // shorter
		// allocate key size
		outputs[i].SPubKey = make([]byte, pksize)
		// read the pubkey
		_, err = buf.Read(outputs[i].SPubKey)
		if err != nil {
			return nil, err
		}
		// extract address, disabled by default
		// invalid transactions (OP_RETURN) produce empty outputs
		if Params.LoadAddr &&
			len(outputs[i].SPubKey) > 0 && outputs[i].SPubKey[0] != OP_RETURN {
			address, outputs[i].LockType, err =
				Pk2Address(outputs[i].SPubKey)
			if err != nil {
				return nil, err
			}
			outputs[i].Address = address[0]
		}
		if Params.LoadRaw {
			outputs[i].Raw, err = readRaw(buf, pos)
			if err != nil {
				return nil, err
			}
		}
	}
	return outputs, nil
}

// Load Segregated Witness data
func (b *Block) readWitness(buf *bytes.Reader, wsize uint64, inputs *Inputs) error {
	// make room for witness
	witness := make([]byte, wsize)
	// read it
	_, err := buf.Read(witness)
	if err != nil {
		return err
	}
	inputs.Witness = append(inputs.Witness, [][]byte{witness}...)
	return nil
}

// WitnessLoop loops through witness fields and loads them
func (b *Block) witnessLoop(buf *bytes.Reader, inputs []Inputs) error {
	// BIP 141
	// Each txin is associated with a witness field.  A witness field
	// starts with a var_int to indicate the number of stack items for
	// the txin. It is followed by stack items, with each item starts
	// with a var_int to indicate the length. Witness data is NOT
	// script.
	for l := range inputs { // loop input number
		swcount, _, err := ReadVarint(buf)
		if err != nil {
			return err
		}
		var k uint64
		// loop through winess fields
		for k = 0; k < swcount; k++ {
			wsize, _, err := ReadVarint(buf)
			if err != nil {
				return err
			}
			err = b.readWitness(buf, wsize, &inputs[l])
			if err != nil {
				return err
			}
		}
		if !Params.LoadInAddr {
			continue
		}
		if inputs[l].Coinbase {
			continue
		}
		// we already have an address loaded, P2SH embedded
		if len(inputs[l].Address) > 0 {
			continue
		}
		// load address
		// pubkey and redeem script are last witness member
		a := make([]string, 1)
		r := inputs[l].Witness[k-1]
		if !ValidPkSize(len(r), r[0]) {
			// P2WSH makes bech32 address from redeem script SHA256
			h := sha256.Sum256(r)
			inputs[l].LockType = "P2WSH"
			a[0], err = MkWitAddr(h[:])
		} else {
			// P2WPKH makes bech32 address from pubkey Hash160
			inputs[l].LockType = "P2WPKH"
			a[0], err = MkWitAddr(btcutil.Hash160(r))
		}
		if err != nil {
			return err
		}
		inputs[l].Address = a
	}
	return nil
}

// ReadTransactions loops through transactions and loads them
// This function receives a byte slice beginning with TxCount, then the
// raw transaction
func (b *Block) ReadTransactions(t []byte) error {
	buf := bytes.NewReader(t)
	var err error
	// read variable integer tx count
	var vsiz int
	b.TxCount, vsiz, err = ReadVarint(buf)
	if err != nil {
		return err
	}

	var i uint64
	var j int
	b.Transac = make([]TransacData, b.TxCount)
	tlen := len(t)

	var chkfunc = func(d TransacData) {
		// optional function to call for each transaction
		if Params.TransacFunc != nil {
			Params.TransacFunc.(func(Block, TransacData))(*b, d)
		}
	}

	for i, j = 0, vsiz; i < b.TxCount; i++ {

		b.Transac[i].SegWit = false

		// transaction version
		err := binary.Read(buf, binary.LittleEndian, &b.Transac[i].Version)
		if err != nil {
			return err
		}

		// save before marker and flag for segwit txid
		curp := tlen - buf.Len() // current position i after signature
		// save 100% raw for display purposes
		raw := make([]byte, len(t[j:curp]))
		copy(raw, t[j:curp]) // signature
		// point to original raw transaction
		rawNoWit := t[j:curp] // signature
		var postFlag int
		// read variable integer inputs count
		b.Transac[i].InCount, _, err = ReadVarint(buf)
		if err != nil {
			return err
		}

		// SegWit marker, not InCount
		if (b.Transac[i].InCount) == 0 {
			flag, err := buf.ReadByte()
			if err != nil {
				return err
			}
			if flag != 0x1 {
				return fmt.Errorf("0 inputs and non SegWit")
			}
			b.Transac[i].SegWit = true
			// strip marker and flag from segwit txid
			postFlag = tlen - buf.Len()
			// re-read variable integer inputs count
			b.Transac[i].InCount, _, err = ReadVarint(buf)
			if err != nil {
				return err
			}
			// add marker and flag the the original raw transaction
			raw = append(raw, []byte{0x0, 0x1}...)
		}
		sw := b.Transac[i].SegWit // shorter
		// read InCount inputs
		b.Transac[i].Ins, err = b.ReadInputs(b.Transac[i].InCount, buf)
		if err != nil {
			return err
		}

		// read variable integer outputs count
		b.Transac[i].OutCount, _, err = ReadVarint(buf)
		if err != nil {
			return err
		}
		b.Transac[i].Outs, err = b.ReadOutputs(b.Transac[i].OutCount, buf)
		if err != nil {
			return err
		}
		// SegWit, load witness fields
		if sw {
			// current place in raw data, just before witness
			curp = tlen - buf.Len()
			// record incount, ins, outcount, outs
			rawNoWit = append(rawNoWit, t[postFlag:curp]...)
			raw = append(raw, rawNoWit[4:]...) // pass signature
			// load witnesses
			err = b.witnessLoop(buf, b.Transac[i].Ins)
			if err != nil {
				return err
			}
		}
		// load lock time
		readbytes := make([]byte, 4)
		_, err = buf.Read(readbytes)
		if err != nil {
			return err
		}
		b.Transac[i].LockTime = binary.LittleEndian.Uint32(readbytes)

		// curp == after version if not segwit, before witness if sw
		raw = append(raw, t[curp:tlen-buf.Len()]...)
		curp = tlen - buf.Len() // new start index
		j = curp
		if Params.LoadTxid { // load TxIds
			// record non sw current txid
			txid := sha256.Sum256(raw)
			txid = sha256.Sum256(txid[:])
			if sw { // SegWit, we need a WTxid (with witnes included) and a Txid
				// append lock time
				rawNoWit = append(rawNoWit, readbytes...)

				nowtxid := sha256.Sum256(rawNoWit)
				nowtxid = sha256.Sum256(nowtxid[:])
				// txid must not contain flag, marker nor witness
				b.Transac[i].Txid = hex.EncodeToString(reverse(nowtxid[:]))
				b.Transac[i].WTxid = hex.EncodeToString(reverse(txid[:]))

				chkfunc(b.Transac[i])
			} else { // non SegWit, txid hashes all transaction
				b.Transac[i].Txid = hex.EncodeToString(reverse(txid[:]))
			}
		}
		// don't show up raw transaction if JSON is displayed and raw flag
		// has not been added
		if Params.LoadRaw == true {
			b.Transac[i].Raw = raw
		}
		// call optional Transaction helper function
		chkfunc(b.Transac[i])
	}
	return nil
}

// readFileByChunks reads the block file by chunks of `bufsiz` in order to
// speed up the process of loading the block file.
// Inspired from from https://kgrz.io/reading-files-in-go-an-overview.html
func readFileByChunks(f string) ([]byte, error) {
	var offlist []int64
	file, err := os.Open(f)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	fileinfo, err := file.Stat()
	if err != nil {
		return nil, err
	}
	filesize := int(fileinfo.Size())

	bufsiz := int(filesize / 10) // split in ten chunks
	data := make([]byte, filesize)
	// Number of go routines we need to spawn.
	concurrency := filesize / bufsiz

	// check for any left over bytes. Add the residual number of bytes as the
	// the last chunk size.
	if remainder := filesize % bufsiz; remainder != 0 {
		concurrency++
	}
	offlist = make([]int64, concurrency)

	for i := 0; i < concurrency; i++ {
		offlist[i] = int64(bufsiz * i)
	}

	var wg sync.WaitGroup
	wg.Add(concurrency)

	errs := make(chan error)

	for i := 0; i < concurrency; i++ {
		go func(x int) {
			defer wg.Done()

			_, err := file.ReadAt(data[offlist[x]:], offlist[x])
			if err != nil {
				errs <- err
			}
			errs <- nil
		}(i)
	}

	go func() {
		wg.Wait()
		close(errs)
	}()

	for e := range errs {
		if e != nil {
			return nil, err
		}
	}
	if len(data) != filesize {
		return nil, fmt.Errorf("wrong data size")
	}

	return data, nil
}

// LoadBlocks reads the blkXXXXX.dat file and returns structured data.
// It can optionally compute and load txid for each transaction, and every
// address involved in inputs and outputs. Those two options are enabled
// by setting txid and addrs to true.
func LoadBlocks(blk string) ([]Block, error) {
	dat, err := readFileByChunks(blk)
	if err != nil {
		return nil, err
	}

	if Params.StartHash > 0 {
		dat = dat[Params.StartHash-8:] //  8 == magic + size
	}

	var rb []Block // blocks array
	var i, n, size, bsize uint32
	size = uint32(len(dat)) // total size of block file

	cb := make(chan Block)
	errs := make(chan error)

	var wg sync.WaitGroup
	// i == block offset
	// bsize == block size
	// size == data size
	// n == iteration
	for i, n = 0, 0; i < size; i, n = i+bsize, n+1 {
		// start block was given, not yet reached
		if Params.StartBlock > 0 && n < Params.StartBlock {
			continue
		}
		// number of blocks given is reached
		if Params.NBlocks > 0 && n >= Params.NBlocks+Params.StartBlock {
			break
		}
		wg.Add(1)
		// block size + magic + size, minus one for the loop count
		bsize = binary.LittleEndian.Uint32(dat[i+4:i+8]) + 8
		// size exposed in block info is greater that the actual data size,
		// this happens in test where the block file size is truncated to 1
		if bsize > size {
			bsize = size
		}
		// spawn one go routine by transaction
		go func(xdat []byte) {
			defer wg.Done()
			var b Block
			//var err error
			b.Magic = binary.LittleEndian.Uint32(xdat[0:4])
			b.Size = binary.LittleEndian.Uint32(xdat[4:8])
			if b.Magic != cmagic["BTC"] {
				err = fmt.Errorf("%v (block %d) is not BTC", b.Magic, i)
				errs <- err
				return
			}
			b.ReadHeader(xdat[8:88])

			if Params.LoadBlockHash { // compute and load current block hash
				h := sha256.Sum256(xdat[8:88])
				h = sha256.Sum256(h[:])
				b.BlockHash = hex.EncodeToString(reverse(h[:]))
			}

			err = b.ReadTransactions(xdat[88:])
			if err != nil {
				errs <- err
				return
			}

			cb <- b
			errs <- nil
		}(dat[i : i+bsize])
	}

	go func() { // wait for the loop goroutines to end and close chans
		wg.Wait()
		close(cb)
		close(errs)
	}()

	for {
		select {
		case err := <-errs:
			if err != nil {
				return nil, err
			}
		case b := <-cb:
			if b.Magic == 0x0 { // closed channel send enpty Block
				if Params.AllBlocksFunc != nil {
					Params.AllBlocksFunc.(func([]Block))(rb)
				}
				return rb, nil
			}
			rb = append(rb, b)
			// optional function to call for each block
			if Params.BlockFunc != nil {
				Params.BlockFunc.(func(Block))(b)
			}
		}
	}
}
