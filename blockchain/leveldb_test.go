package blockchain

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"log"
	"testing"

	"gitlab.com/iMil/goxplorer/gendb"
)

const (
	index     = "../samples/testindex"
	hash      = "00000000000000000000a52d7610399a31757412014f0c46f44922b3d6afddee"
	indexdata = "89fe04a3f309801d88218d4ed2a44189bd1300000020ab657abc5a0388c5e47f581f89e7e2080284962214de12000000000000000000035518dd2cc2560b24f7c69141cd60ad5fc6fdc24e491a32aded90ba53b71d658d9dd55dd120161720bbb7f4"
	blkfile   = 1870
	position  = 1364673
	height    = 604681
	date      = "2019-11-21"
)

func fatalErr(err error, t *testing.T) {
	if err != nil {
		t.Fatal(err)
	}
}

func TestFileRecordByFile(t *testing.T) {
	keyTable := []struct {
		key   string
		param string
		want  string
	}{
		{"f", "1845", "7fbebbc82287cf9112a3d431a3db7f84ecc1dc4684ece88220"},
		{"b", hash, indexdata},
	}
	db, err := gendb.NewDB(gendb.LevelDB, index)
	fatalErr(err, t)
	defer db.Close()
	var data []byte
	for _, tt := range keyTable {
		t.Run(fmt.Sprintf("%s-%s", tt.key, tt.param), func(t *testing.T) {
			data, err = GetKeyVal(db, tt.key, tt.param)
			fatalErr(err, t)
			want, err := hex.DecodeString(tt.want)
			fatalErr(err, t)
			if !bytes.Equal(data, want) {
				t.Fatal("wrong value returned")
			}
		})
	}
}

func testReadRecord(by int, t *testing.T) {
	var data []byte
	db, err := gendb.NewDB(gendb.LevelDB, index)
	fatalErr(err, t)
	defer db.Close()
	if by == 0 {
		data, err = GetKeyVal(db, "b", hash)
	}
	if by == 1 {
		data, err = GetBlockRecordByHeight(db, height)
	}
	fatalErr(err, t)
	bindex, err := hex.DecodeString(indexdata)
	fatalErr(err, t)
	if !bytes.Equal(data, bindex) {
		t.Fatalf("wrong index record")
	}
	blk, err := ReadRecord(data, BlockRecords, "nFile")
	fatalErr(err, t)
	if blk != blkfile {
		t.Fatalf("wrong blockfile")
	}
	dpos, err := ReadRecord(data, BlockRecords, "nDataPos")
	fatalErr(err, t)
	if dpos != position {
		t.Fatalf("wrong position")
	}
}

func TestReadRecordByHash(t *testing.T) {
	testReadRecord(0, t)
}

func TestReadRecordByHeight(t *testing.T) {
	testReadRecord(1, t)
}

func TestReadFileByDate(t *testing.T) {
	db, err := gendb.NewDB(gendb.LevelDB, index)
	fatalErr(err, t)
	defer db.Close()
	blk, err := GetFileRecordByDate(db, date)
	log.Println(blk)
	if blk != blkfile {
		t.Fatal("wrong file number")
	}
}

func TestGetLDBBlock(t *testing.T) {
	db, err := gendb.NewDB(gendb.LevelDB, index)
	fatalErr(err, t)
	defer db.Close()

	var tb TBlockRecords
	tb, err = GetLDBBlock(db, hash)
	fatalErr(err, t)
	if tb.NHeight != 604681 {
		log.Fatal("wrong height returned")
	}
}

func TestGetLDBFile(t *testing.T) {
	db, err := gendb.NewDB(gendb.LevelDB, index)
	fatalErr(err, t)
	defer db.Close()

	var tf TFileRecords
	tf, err = GetLDBFile(db, "1845")
	fatalErr(err, t)
	if tf.NHeightFirst != 600753 {
		log.Fatal("wrong nsize returned")
	}
}
